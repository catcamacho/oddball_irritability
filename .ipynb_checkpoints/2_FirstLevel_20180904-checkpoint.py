
# coding: utf-8

# In[ ]:


from os.path import join
from nipype.pipeline.engine import Workflow, Node, MapNode
from nipype.interfaces.utility import IdentityInterface, Function
from nipype.interfaces.io import SelectFiles, DataSink
from nipype.algorithms.modelgen import SpecifyModel
from nipype.interfaces.fsl.model import Level1Design, FEATModel, GLM
from nipype.interfaces.fsl.preprocess import FLIRT, SUSAN

# FSL set up- change default file output type
from nipype.interfaces.fsl import FSLCommand
FSLCommand.set_default_output_type('NIFTI_GZ')

# Study-specific variables
project_home = '/home/camachocm2/Analysis/CARS_oddball'
timing_dir = project_home + '/timing/'
output_dir = project_home + '/proc/firstlevel'
preproc_dir = project_home + '/proc/preprocessing'
wkflow_dir = project_home + '/workflows'
subjects_list = open(project_home + '/misc/subjects.txt').read().splitlines()
#subjects_list = ['101']

#conditionNames = ['oddball']
#con01 = ['oddball', 'T',conditionNames, [1]]
#contrasts_list = [con01]

conditionNames = ['Angry','Happy','Neutral']

con01 = ['Angry', 'T',conditionNames, [1, 0, 0]]
con02 = ['Happy', 'T', conditionNames, [0, 1, 0]]
con03 = ['Neutral', 'T', conditionNames, [0, 0, 1]]
con04 = ['Angry > Neutral', 'T', conditionNames, [1, 0, -1]]
con05 = ['Happy > Neutral', 'T', conditionNames, [0, 1, -1]]
con06 = ['Angry > Happy', 'T', conditionNames, [1, -1, 0]]

contrasts_list = [con01, con02, con03, con04, con05, con06]
# MNI Space brain templates
MNI_brain = project_home + '/template/MNI152_T1_2mm_brain.nii.gz'
MNI_brainmask = project_home + '/template/MNI152_T1_2mm_brainmask.nii.gz'
TR=2 # in seconds


# In[ ]:


## Data handling nodes

# Select subjects list
infosource = Node(IdentityInterface(fields=['subjid']),
                  name='infosource')
infosource.iterables = [('subjid', subjects_list)]

# Select files
# Select files
templates = {'timing': timing_dir + '/OddballOnsets_{subjid}.txt',
             'outliers': preproc_dir + '/art_outliers/{subjid}/art.oddball_raw_reoriented_st_mcf_flirt_outliers.txt', 
             'motion':preproc_dir + '/mcflirt_displacement/{subjid}/oddball_raw_reoriented_st_mcf.nii.gz.par',
             'func': preproc_dir + '/coreg_func/{subjid}/oddball_raw_reoriented_st_mcf_flirt.nii.gz',
             'anat': preproc_dir + '/reoriented_anat/{subjid}/brainmask_out_reoriented.nii.gz'}

selectfiles = Node(SelectFiles(templates), 
                   name='selectfiles')

# Sink data of interest (mostly for QC)
substitutions = [('_subjid_', '')] #output file name substitutions
datasink = Node(DataSink(base_directory = output_dir,
                        container = output_dir,
                        substitutions = substitutions), 
                name='datasink')


# In[ ]:


## Timing handling nodes

def oddballTiming(timing):
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    
    from numpy import genfromtxt
    from nipype.interfaces.base import Bunch
    from pandas import Series, DataFrame, read_table
    
    conditionNames = ['Angry','Happy','Neutral']
    allTrials = genfromtxt(timing, delimiter='\t',
                           dtype=None, skip_header = 1)
    onsets1 = []
    onsets2 = []
    onsets3 = []
     
    # get onsets for distraction
    for t in allTrials:
        if 'Angry' in t[5]:
            onsets1.append(t[0])
        elif 'Happy' in t[5]:
            onsets2.append(t[0])
        elif 'Neutral' in t[5]:
            onsets3.append(t[0])
            
    onsets = [onsets1,onsets2,onsets3]
    durations = [[2],[2],[2]]
    
    tasktiming = []
    tasktiming.insert(0,Bunch(conditions=conditionNames,
                              onsets=onsets,
                              durations=durations,
                              amplitudes=[[1],[1],[1]]))

    return(tasktiming)


# In[ ]:


## Level 1 Nodes: FSL versions

getSubjTaskInfo = Node(name='getSubjTaskInfo',
                       interface=Function(input_names=['timing'],
                                          output_names=['tasktiming'],
                                          function=oddballTiming))

# Specify FSL model
modelspec = Node(SpecifyModel(time_repetition=TR, 
                              high_pass_filter_cutoff=128, 
                              input_units='secs',
                              parameter_source='FSL'),
                 name='modelspec')

# Generate a level 1 design
level1design = Node(Level1Design(bases={'dgamma':{'derivs': False}},
                                 interscan_interval=TR, # the TR
                                 model_serial_correlations=True,
                                 contrasts=contrasts_list), 
                    name='level1design')

# Estimate Level 1
generateModel = Node(FEATModel(), 
                     name='generateModel')

#run the GLM
estmodel = Node(GLM(out_file = 'betas.nii.gz', 
                    out_cope='cope.nii.gz',
                    out_t_name = 'tstat.nii.gz'), 
                name= 'estmodel')


# In[ ]:


#Normalize to MNI space
norm_anat = Node(FLIRT(reference=MNI_brain),
               name='norm_anat')

def converthex_xform(orig_xform):
    from numpy import genfromtxt, savetxt
    from os.path import abspath
    
    orig_matrix = genfromtxt(orig_xform, delimiter='  ',
                             dtype=None, skip_header=0)
    new_xform = 'brainmask_out_flirt.mat'
    savetxt(new_xform, orig_matrix, delimiter='  ')
    
    xform_file = abspath(new_xform)
    return(xform_file)

converthex = Node(name='converthex', 
                  interface=Function(input_names=['orig_xform'], 
                                     output_names=['xform_file'], 
                                     function=converthex_xform))

norm_copes = MapNode(FLIRT(apply_xfm=True,
                           reference=MNI_brain),
                     iterfield=['in_file'],
                     name='norm_copes')

norm_raw = Node(FLIRT(apply_xfm=True,
                      reference=MNI_brain, 
                      out_file='norm_func.nii.gz'),
                name='norm_raw')

# Smooth Data
def brightthresh(func):
    import nibabel as nib
    from numpy import median, where
    
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    
    func_nifti1 = nib.load(func)
    func_data = func_nifti1.get_data()
    func_data = func_data.astype(float)
    
    brain_values = where(func_data > 0)
    median_thresh = median(brain_values)
    bright_thresh = 0.75 * median_thresh
    
    return(bright_thresh)
    
brightthresh_copes = MapNode(name='brightthresh_copes',
                             interface=Function(input_names=['func'],
                                         output_names=['bright_thresh'],
                                         function=brightthresh),
                            iterfield=['func'])

brightthresh_raw = Node(name='brightthresh_raw',
                        interface=Function(input_names=['func'],
                                           output_names=['bright_thresh'],
                                           function=brightthresh))

smooth_copes = MapNode(SUSAN(fwhm=6), 
                       iterfield=['in_file', 'brightness_threshold'],
                       name='smooth_copes')

smooth_raw = Node(SUSAN(fwhm=6),
                  iterfield=['in_file','brightness_threshold'],
                  name='smooth_raw')


# In[ ]:


# QC the outputs
def create_brain_outlines(target,comparison):
    import os
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    from nilearn import plotting
    
    brainoutline_filename='registration_check.png'
    display = plotting.plot_anat(target, display_mode='x',
                                 cut_coords=4,
                                 draw_cross=False)
    display.add_edges(comparison)
    display.savefig(brainoutline_filename)
    display.close()
    outlined_brain_file = os.path.abspath(brainoutline_filename)

    return(outlined_brain_file)

check_registration = Node(name='check_registration',
                      interface=Function(input_names=['target','comparison'],
                                         output_names=['outlined_brain_file'],
                                         function=create_brain_outlines))
check_registration.inputs.target = MNI_brain


# In[ ]:


## Level 1 workflow

L1workflow = Workflow(name='L1workflow')
L1workflow.connect([(infosource,selectfiles,[('subjid','subjid')]),
                    (selectfiles,getSubjTaskInfo,[('timing','timing')]),
                    (selectfiles,getSubjTaskInfo,[('motion','motion_file')]), 
                    (selectfiles,modelspec,[('func','functional_runs')]),
                    (selectfiles,modelspec,[('outliers','outlier_files')]),
                    (selectfiles,modelspec,[('motion','realignment_parameters')]),
                    (getSubjTaskInfo,modelspec,[('tasktiming','subject_info')]),
                    (modelspec,level1design,[('session_info','session_info')]),
                    (level1design,generateModel,[('ev_files','ev_files')]),
                    (level1design,generateModel,[('fsf_files','fsf_file')]),
                    (generateModel,estmodel,[('design_file','design')]),
                    (generateModel,estmodel,[('con_file','contrasts')]),
                    (selectfiles,estmodel,[('func','in_file')]),
                    
                    (selectfiles,norm_anat,[('anat','in_file')]),
                    (norm_anat,converthex,[('out_matrix_file','orig_xform')]),
                    (converthex,norm_copes,[('xform_file','in_matrix_file')]),
                    (converthex,norm_raw,[('xform_file','in_matrix_file')]),
                    (estmodel,norm_copes,[('out_cope','in_file')]),
                    (selectfiles,norm_raw,[('func','in_file')]), 
                    (norm_copes, brightthresh_copes, [('out_file','func')]),
                    (norm_raw, brightthresh_raw, [('out_file','func')]),
                    (brightthresh_copes, smooth_copes, [('bright_thresh','brightness_threshold')]),
                    (brightthresh_raw, smooth_raw, [('bright_thresh','brightness_threshold')]),
                    (norm_copes, smooth_copes, [('out_file','in_file')]),
                    (norm_raw, smooth_raw, [('out_file','in_file')]),
                    (norm_anat, check_registration, [('out_file','comparison')]),
                    
                    (check_registration, datasink, [('outlined_brain_file','check_reg_MNI')]),
                    (generateModel, datasink, [('design_image','design_image')]),
                    (smooth_copes, datasink, [('smoothed_file','smoothed_copes')]),
                    (smooth_raw, datasink, [('smoothed_file','smoothed_raw')])
                   ])
L1workflow.base_dir = join(wkflow_dir)
L1workflow.write_graph(graph2use='flat')
L1workflow.run('MultiProc', plugin_args={'n_procs': 4, 'memory_gb':10})

