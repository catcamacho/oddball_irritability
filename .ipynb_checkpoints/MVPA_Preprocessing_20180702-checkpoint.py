
# coding: utf-8

# # Preprocessing for machine learning
# This notebook is designed to preprocess  neuroimaging and behavioral data for machine learning analyses. This includes mean-centering and normalizing vector data (i.e. questionnaire scores, demographics) and extracting a beta series from the fMRI data. The beta series is a result of deconvolving the time series for each trial which allows us to use the beta maps as entries for classification (see Mumford 2012 for a more thorough explanation of this; these steps are also outlined below).

# In[ ]:


from pandas import DataFrame, Series, read_csv

# Study specific variables
study_home = '/home/camachocm2/Analysis/CARS_oddball/mvpa_proc'
sub_data_file = study_home + '/subjectinfo.csv'

subject_info = read_csv(sub_data_file, index_col=None)
subjects_list = subject_info['subjID'].tolist()
subjects_list = [str(a) for a in subjects_list]
#subjects_list=['101']


# ### fMRI data preprocessing

# In[ ]:


from nipype.pipeline.engine import Workflow, Node, MapNode
from nipype.interfaces.fsl import Merge, ImageMeants, Split, MotionOutliers, SUSAN, SliceTimer, MCFLIRT, FLIRT, Reorient2Std
from nipype.algorithms.modelgen import SpecifyModel
from nipype.interfaces.nipy.model import FitGLM, EstimateContrast
from nipype.interfaces.utility import IdentityInterface, Function
from nipype.interfaces.io import SelectFiles, DataSink, DataGrabber, FreeSurferSource
from nipype.interfaces.fsl.model import GLM, Level1Design, FEATModel
from nipype.interfaces.nipy.preprocess import Trim
from nipype.interfaces.freesurfer import FSCommand, MRIConvert


raw_dir = study_home + '/raw'
output_dir = study_home + '/processed_data'
timing_dir = study_home + '/timing'
workflow_dir = study_home + '/workflows'

# MNI Space brain templates
MNI_brain = study_home + '/template/MNI152_T1_2mm_brain.nii.gz'
MNI_brainmask = study_home + '/template/MNI152_T1_2mm_brainmask.nii.gz'

TR=2 #in seconds

# FSL set up- change default file output type
from nipype.interfaces.fsl import FSLCommand
FSLCommand.set_default_output_type('NIFTI_GZ')

#freesurfer setup
fs_dir = '/moochie/Cat/Aggregate_anats/subjects_dir'
FSCommand.set_default_subjects_dir(fs_dir)


# In[ ]:


# Data handling nodes
infosource = Node(IdentityInterface(fields=['subjid']), 
                  name='infosource')
infosource.iterables = [('subjid', subjects_list)]
infosource.synchronize = True

template = {'raw_func': raw_dir + '/{subjid}/oddball_raw.nii.gz', 
            'timing':timing_dir + '/OddballOnsets_{subjid}.txt'}
selectfiles = Node(SelectFiles(template), name='selectfiles')

substitutions = [('_subjid_', '')]
datasink = Node(DataSink(substitutions=substitutions, 
                         base_directory=output_dir,
                         container=output_dir), 
                name='datasink')

# FreeSurferSource - Data grabber specific for FreeSurfer data
fssource = Node(FreeSurferSource(subjects_dir=fs_dir),
                run_without_submitting=True,
                name='fssource')


# In[ ]:


# Extract timing for Beta Series Method- mark trials as high and low motion
def pull_timing(timing_file,motion_file):
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    
    from pandas import DataFrame,Series,read_table
    from nipype.interfaces.base import Bunch
    
    motion_series = read_table(motion_file, header=None, names=['fd'])
    motion = motion_series['fd'].tolist()
    
    timing = read_table(timing_file)
    timing = timing[timing['ConditionNum']<4]
    timing = timing.sort_values('ConditionName')

    names = timing['ConditionName'].tolist()
    cond_names = [names[a] + str(a) for a in range(0,len(names))]
    
    onsets = []
    for a in timing['PictureOnset'].tolist():
        onsets.append([a])
        
    durations = []
    for b in range(0,24):
        durations.append([2])
    
    #make bunch file
    timing_bunch = []
    timing_bunch.insert(0,Bunch(conditions=cond_names,
                                onsets=onsets,
                                durations=durations,
                                amplitudes=None,
                                tmod=None,
                                pmod=None,
                                regressor_names=['fd'],
                                regressors=[motion]))
    return(timing_bunch)

# Function to create contrast lists from a bunch file
def beta_contrasts(timing_bunch):
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    
    from nipype.interfaces.base import Bunch
    from numpy import zeros
    
    conditions_names = timing_bunch[0].conditions
    
    # Make the contrast vectors for each trial
    boolean_con_lists = []
    num_cons = len(conditions_names)
    for i in range(0,num_cons):
        boo = zeros(num_cons)
        boo[i] = 1
        boolean_con_lists.append(list(boo))
    
    # Create the list of lists for the full contrast info
    contrasts_list = []
    for a in range(0,num_cons):
        con = (conditions_names[a], 'T', conditions_names, boolean_con_lists[a])
        contrasts_list.append(con)
    
    return(contrasts_list)

# Smooth Data
def brightthresh(func):
    import nibabel as nib
    from numpy import median, where
    
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    
    func_nifti1 = nib.load(func)
    func_data = func_nifti1.get_data()
    func_data = func_data.astype(float)
    
    brain_values = where(func_data > 0)
    median_thresh = median(brain_values)
    bright_thresh = 0.75 * median_thresh
    
    return(bright_thresh)


# In[ ]:


# Data QC nodes
def create_coreg_plot(epi,anat):
    import os
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    from nilearn import plotting
    
    coreg_filename='coregistration.png'
    display = plotting.plot_anat(epi, display_mode='ortho',
                                 draw_cross=False,
                                 title = 'coregistration to anatomy')
    display.add_edges(anat)
    display.savefig(coreg_filename) 
    display.close()
    coreg_file = os.path.abspath(coreg_filename)
    
    return(coreg_file)

def check_mask_coverage(epi,brainmask):
    from os.path import abspath
    from nipype import config, logging
    config.enable_debug_mode()
    logging.update_logging(config)
    from nilearn import plotting
    from nipype.interfaces.nipy.preprocess import Trim
    
    trim = Trim()
    trim.inputs.in_file = epi
    trim.inputs.end_index = 1
    trim.inputs.out_file = 'epi_vol1.nii.gz'
    trim.run()
    epi_vol = abspath('epi_vol1.nii.gz')
    
    maskcheck_filename='maskcheck.png'
    display = plotting.plot_anat(epi_vol, display_mode='ortho',
                                 draw_cross=False,
                                 title = 'brainmask coverage')
    display.add_contours(brainmask,levels=[.5], colors='r')
    display.savefig(maskcheck_filename)
    display.close()
    maskcheck_file = abspath(maskcheck_filename)

    return(maskcheck_file)

make_coreg_img = Node(name='make_coreg_img',
                      interface=Function(input_names=['epi','anat'],
                                         output_names=['coreg_file'],
                                         function=create_coreg_plot))

make_checkmask_img = Node(name='make_checkmask_img',
                      interface=Function(input_names=['epi','brainmask'],
                                         output_names=['maskcheck_file'],
                                         function=check_mask_coverage))
make_checkmask_img.inputs.brainmask = MNI_brainmask


# In[ ]:


# Convert skullstripped brain to nii, resample to 2mm^3
resample = Node(MRIConvert(out_type='niigz',
                          vox_size=(2,2,2)),
               name='resample')

# Reorient anat to MNI space
reorient_anat = Node(Reorient2Std(),
                     name='reorient_anat')

# reorient images to MNI space standard
reorient_func = Node(Reorient2Std(),
                     name='reorient_func')

# perform slice time correction
slicetime = Node(SliceTimer(index_dir=False,
                           interleaved=True,
                           time_repetition=TR),
                name='slicetime')

# rigid realignment
realignmc = Node(MCFLIRT(save_plots=True),
                name='realignmc')

# Smoothing
brightthresh = Node(Function(input_names=['func'],
                             output_names=['bright_thresh'],
                             function=brightthresh),
                    name='brightthresh')

smooth = Node(SUSAN(fwhm=6, out_file='processed_func.nii.gz'),
              name='smooth')

# Coregistration
coregflt = Node(FLIRT(),
               name='coregflt')
coregflt2 = Node(FLIRT(apply_xfm=True),
                name='coregflt2')

#Normalize to MNI space
norm_anat = Node(FLIRT(reference=MNI_brain),
               name='norm_anat')
norm_func = Node(FLIRT(apply_xfm=True,
                       reference=MNI_brain),
                 name='norm_func')

# Get framewise displacement to use as a regressor in the GLM
get_fd = Node(MotionOutliers(metric='fd',
                             out_metric_values='FD.txt', 
                             out_metric_plot='FD.png'),
              name='get_fd')


# In[ ]:


# Extract timing
pull_timing = Node(Function(input_names=['timing_file','motion_file'],
                            output_names=['timing_bunch'],
                            function=pull_timing), 
                   name='pull_timing')

# create the list of T-contrasts
define_contrasts = Node(Function(input_names=['timing_bunch'], 
                                 output_names = ['contrasts_list'], 
                                 function=beta_contrasts),
                        name = 'define_contrasts')

# Specify FSL model - input bunch file called subject_info
modelspec = Node(SpecifyModel(time_repetition=TR, 
                              input_units='secs',
                              high_pass_filter_cutoff=128),
                 name='modelspec')

# Generate a level 1 design
level1design = Node(Level1Design(bases={'dgamma':{'derivs': False}},
                                 interscan_interval=TR, 
                                 model_serial_correlations=True), 
                    name='level1design')

# Estimate Level 1
generate_model = Node(FEATModel(), 
                      name='generate_model')

# Run GLM
extract_pes = Node(GLM(out_file = 'betas.nii.gz'), 
                   name='extract_pes')

# Trim the nuissance regressor from the betas (the motion regressor)
trim_pes = Node(Trim(end_index = 24, out_file='betas.nii.gz'), 
                name='trim_pes')


# In[ ]:


preprocflow = Workflow(name='preprocflow')
preprocflow.connect([(infosource, selectfiles,[('subjid','subjid')]),
                     (infosource, fssource, [('subjid','subject_id')]),
                     (selectfiles, get_fd, [('raw_func','in_file')]),
                     (selectfiles, pull_timing, [('timing','timing_file')]),
                     (selectfiles, reorient_func, [('raw_func','in_file')]),
                     (reorient_func, slicetime, [('out_file','in_file')]),
                     (slicetime, realignmc, [('slice_time_corrected_file','in_file')]),
                     
                     (realignmc, coregflt, [('out_file','in_file')]),
                     (realignmc, coregflt2, [('out_file','in_file')]),
                     (fssource, resample, [('brainmask','in_file')]),
                     (resample, reorient_anat, [('out_file','in_file')]),
                     (reorient_anat, coregflt, [('out_file','reference')]),
                     (coregflt, coregflt2, [('out_matrix_file','in_matrix_file')]),
                     (reorient_anat, coregflt2, [('out_file','reference')]),
                     (reorient_anat, norm_anat, [('out_file','in_file')]),
                     (coregflt2, norm_func, [('out_file','in_file')]),
                     (norm_anat, norm_func, [('out_matrix_file','in_matrix_file')]),
                     (norm_func, brightthresh, [('out_file','func')]),
                     (brightthresh, smooth, [('bright_thresh','brightness_threshold')]),
                     (norm_func, smooth, [('out_file','in_file')]),
                     
                     (coregflt, make_coreg_img, [('out_file','epi')]),
                     (reorient_anat, make_coreg_img, [('out_file','anat')]),
                     (norm_func, make_checkmask_img, [('out_file','epi')]),
                     
                     (smooth, modelspec, [('smoothed_file','functional_runs')]),
                     (get_fd, pull_timing, [('out_metric_values','motion_file')]),
                     (pull_timing, modelspec, [('timing_bunch','subject_info')]),
                     (pull_timing, define_contrasts, [('timing_bunch','timing_bunch')]),
                     (define_contrasts, level1design, [('contrasts_list','contrasts')]),
                     (modelspec, level1design, [('session_info','session_info')]),
                     (level1design,generate_model, [('ev_files','ev_files')]),
                     (level1design,generate_model, [('fsf_files','fsf_file')]),
                     (generate_model,extract_pes, [('design_file','design')]),
                     (generate_model,extract_pes, [('con_file','contrasts')]),
                     (smooth,extract_pes, [('smoothed_file','in_file')]),
                     (extract_pes, trim_pes, [('out_file','in_file')]),
                     
                     (smooth, datasink, [('smoothed_file','preprocessed_func')]),
                     (norm_anat, datasink, [('out_file','preprocessed_anat')]),
                     (make_checkmask_img, datasink, [('maskcheck_file','mask_image')]),
                     (make_coreg_img, datasink, [('coreg_file','coreg_image')]),
                     (trim_pes,datasink,[('out_file','betas')]),
                     (get_fd, datasink, [('out_metric_values','fd_motion')]),
                     (get_fd, datasink, [('out_metric_plot','fd_motion_plots')]),
                     (generate_model,datasink,[('design_image','design_image')])
                    ])
preprocflow.base_dir = workflow_dir
preprocflow.write_graph(graph2use='flat')
preprocflow.run('MultiProc', plugin_args={'n_procs': 4,'memory_gb':10})
